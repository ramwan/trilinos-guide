This is a condensed rundown of Trilinos going through only the real basics and 
stuff that I've personally had to use. This is by no means meant to be a full 
representation of the capabilities of the framework! There's just too much. This 
should be a good starting point so that the documentation and tests/examples 
provided by the framework maintainers start to make sense though.

The chapters and subchapters are labelled in the order I wrote them so going 
through them in that order also works but feel free to skip sections you know 
or deem not relevant.
