This section is going to be less of a tutorial and more of some pointers on 
how Tpetra behaves and what happens if you do certain things, hopefully to serve 
as a debugging reference in the future. Some of these pointers may be actually 
very uncommon and the situation might be pretty hard to get yourself into but 
they exist here nonetheless.

* You can actually fill a matrix only on a single processor via calls to 
`insertGlobalValue()` and then have `fillComplete()` notify the other processors 
of what filling needs to be done. This may not be as fast as doing a parallel 
fill though.

```cpp
GO total = ...;
GO base = 0;
size_t ents_per_row = ...;
auto comm = Tpetra::getDefaultComm();

RCP<map_type> contigmap = rcp(new map_type(total, base, comm));
RCP<matrix_type> mat = rcp(new matrix_type(contigmap, ents_per_row));

if(comm->getRank() == 0) // inserting only on the first MPI processor
{
  for(GO i=0; i<total; i++)
  {
    mat->insertGlobalValue(....);
  }
}
mat->fillComplete(); // now all processors will have the relevant matrix entries
```

Where in the above snippet, the ellipsis indicates just some relevant value.

* I want to pass around my Tpetra objects from processor to processor. Can I do 
that? Yes of course! Check out [this](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/Tpetra_Lesson05.html) tutorial.

* I called `apply()` or the like but only half my solution vector is populated? 
This might be because you called the function within a block that limited it to 
a single processor. For example,

```cpp
// RCP<matrix_type> mat;
// RCP<vector_type> v;
// RCP<vector_type> result;

if(myRank == 0) mat->apply(*v, *result);
```

In the above snippet, only the elements in "result" owned by processor rank 0 
would have the result.

* I've called `fillComplete()` but Tpetra still complains when I start using my 
matrix! This could be a symptom of the exact same mistake as the above point. 
Check that your fillCompletes are going to be called across all processors and 
you call it again after you do any resumeFills.

* My program is taking an unreasonably long time to finish and I suspect 
something's wrong. This may be due to Tpetra stalling and being unable to perform 
some operations aross multiple processors. One way to test if this is the case 
is to run your program with a single processor (mpirun -np 1) or even without 
mpi and see if it completes. Here's an example of a program that will stall:

```cpp
#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>

int main(int argc, char *argv[])
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef Tpetra::Vector<>::scalar_type Scalar;
  typedef Tpetra::Vector<>::local_ordinal_type LO;
  typedef Tpetra::Vector::global_ordinal_type GO;
  typedef Tpetra::Vector<> vector_type;
  typedef Tpetra::CrsMatrix<> matrix_type;
  
  Tpetra::ScopeGuard tpetraScope(&argc, &argv)
  {
    auto comm = Tpetra::getDefaultComm();
    const size_t myRank = comm->getRank();
    const GO total = 50;
    const GO base = 0;
    const size_t ents_per_row = 1;

    RCP<map_type> contigmap = rcp(new map_type(total, base, comm));
    RCP<vector_type> v = rcp(new vector_type(contigmap));
    RCP<vector_type sol = rcp(new vector_type(contigmap));
    
    // the following if statement is the guilty block.
    if(myRank == 0)
    {
      mat->fillComplete();
      mat->apply(*v, *sol);
    }

    /* If instead of the above we did the following, it would work but we 
       wouldn't get a complete solution.

    mat->fillComplete();
    if(myRank == 0) mat->apply(*v, *sol);
    */
  }
  return 0;
}
```
