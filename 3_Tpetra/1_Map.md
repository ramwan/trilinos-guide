Tpetra - one of the linear algebra library within Trilinos.

The first class to be aware of in Tpetra is the Map class as it describes how 
entries in a linalg object are distributed across processes. The Map class is 
templated on a few things:

```cpp
template<class LocalOrdinal, class GlobalOrdinal, class Node>
class Tpetra::Map<LocalOrdinal, GlobalOrdinal, Node>
```

The LocalOrdinal type must be an int whereas GlobalOrdinal can be int, long or 
long long. The Node parameter eludes me, but will have something to do with 
whether your Trilinos compilation allows parallelisation or MPI etc. 
For all my code, I have a typedef like `typedef Tpetra::Vector<>::node_type 
NT;` which I pass to anything requiring a 'Node' parameter.

```cpp
#include <Tpetra_Core.hpp>

typedef Tpetra::Vector<>::node_type NT;

int main(int argc, char* argv[])
{
  using Tpetra::Map;
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef int LO;
  typedef int GO;

  // We need to include the scopeguard line which initialises Kokkos and mpi 
  // relevant things. We also need to ensure that all our operations involving 
  // mpi, kokkos, tpetra etc. happen within this scope.
  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    RCP<Map<LO, GO, NT> > map = rcp(new Tpetra::Map<LO, GO, NT>()); // do nothing constructor
  }
}
```

The Map class lets us specify to Tpetra how we want our linear algebra object to 
be distributed across our processes. The simplest cases are a contiguous, equal 
distribution or a fully replicated object. What contiguous means in this case is 
that process 0 has elements 0->n-1, process 1 has elements n->2n-1 and so on. 
These process numbers are assigned via MPI and we can access them via the 
MPI communicator object we get from Tpetra.

```cpp
#include <Tpetra_Core.hpp>

typedef Tpetra::Vector<>::node_type NT;

int main(int argc, char* argv[])
{
  using Tpetra::Map;
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef int LO;
  typeded int GO;

  const GO total = 500; // total number of elements
  const GO base = 0; // index base, always going to be 0 here

  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm(); // MPI communicator

    // globally distributed map, each process has equal amounts of rows in a matrix
    // with the excess rows going to the last process.
    RCP<Map<LO, GO, NT> > contigmap = rcp(new Tpetra::Map<LO, GO, NT>(total, base, comm));

    // each process in the mpi group will have its own full copy of an object
    RCP<Map<LO, GO, NT> > uniquemap = rcp(new Tpetra::Map<LO, GO, NT>(total, total, base, comm));
  }
}
```

The above examples show the 2 most common use cases for a Map. You are able to 
more granularly specify which processes own which rows and columns if you want - 
refer to the Tpetra docs to see the full list of constructors.

[Tpetra Map documentation](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/classTpetra_1_1Map.html)
