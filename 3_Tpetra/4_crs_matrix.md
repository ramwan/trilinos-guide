CrsMatrix is Tpetra's implementation of a sparse matrix, templated on the same 
parameters as vectors - Scalar, LocalOrdinal, GlobalOrdinal and Node types. The 
same principles with local indices (local ordinal) and global indices (global 
ordinal), specifying how our object is distributed via the Map object and 
printing via `describe()` (but there's no such thing as `get2dView()` for a 
CrsMatrix).

Here's an example of constructing a CrsMatrix.

```cpp
#include <Tpetra_Core.hpp>
#include <Kokkos_Core.hpp>

int main(int argc, char* argv[])
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Tpetra::Map;
  typedef int LO
  typedef long GO;
  typedef double Scalar;
  typedef Tpetra::Vector<>::node_type NT;
  // here's how CrsMatrix is templated
  typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;

  const GO total = 500; // total number of entries
  const GO base = 0; // index base of arrays

  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    // mpi communicator
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm();
    // contiguous map with 'total' number of entries
    RCP<Map<LO, GO, NT> > contigmap = rcp(new Map<LO, GO, NT>(total, base, comm));

    const size_t ents_per_row = 3;
    // Create our sparse matrix with a row distribution specified by our contigmap 
    // with maximum ents_per_row entries per row. This doesn't mean we HAVE 
    // to have ents_per_row entries per row, just that we can't have more.
    RCP<matrix_type> mat = rcp(new matrix_type(contigmap, ents_per_row));

    // Here's an example of creating the identity matrix.
    for(LO i=0; i<=contigmap->getMaxLocalIndex(); i++)
    {
      // convert from local index to global index
      GO g = contigmap->getGlobalElement(i);
      const Scalar vals[] = {1};
      const GO cols[] = {g};

      // we want to put 1 element into row 'g' with values at columns specified 
      // in vals and cols. As an aside, this is the "old way" of passing 
      // arguments to `insertGlobalValue` but the "new way" uses Teuchos::ArrayView
      // which is so obscenely verbose that I'd say don't even bother with it.
      mat->insertGlobalValue(g, static_cast<GO>(1), vals, cols);
    }
    mat->fillComplete();
  }

  return 0;
}
```

Note the extra line `mat->fillComplete();`. You have to call fillComplete() on a 
matrix before you can start using it otherwise Tpetra will complain. To make 
any additional modifications to the matrix, you can call `resumeFill()` and once 
you're done with the changes, call `fillComplete()` again.

At this point I'm sure that you'd now want to know how and what objects you can 
do things with. For example you want to do a matrix-vector multiply so can you 
just go `my_matrix.apply(vector)` (by the way this isn't the exactly function 
but you get the point)? Well yes but it requires a little bit of extra clarity. 
For example what if your vector and matrices have different row distributions 
across your process pool? Another issue is what if we just have the wrong number 
of entries in our vector so an apply doesn't make sense? Also since we can 
specify a row mapping, can we specify a column mapping too? We've also been able 
to use `insertLocalValue()` for vectors, can we do that for matrices too?

The answer to the last two questions is yes absolutely, but they are interlinked. 
In the code snippet above as it stands, if we just do all the relevant type 
replacing to try and use `insertLocalValue()`, it will fail. To do local value 
inserts, we need to create a column map for the matrix and pass it to the 
constructor argument. The default column map in the above case if we didn't pass 
the constructor one would be the same as the row map so to allow us to use 
`insertLocalValue()`, we can create our matrix with the following constructor.

```cpp
RCP<matrix_type> mat_with_column_map = 
  rcp(new matrix_type(contigmap, contigmap, ents_per_row));

// insertLocalValue() now is allowed
```

What about matrix and vector objects with different row distributions? What 
happens if we run the following sample.

```cpp
// we have a contiguously distributed map and a fully replicated map where each 
// processor contains all the elements
RCP<Map<LO, GO, NT> > contigmap = rcp(new Map<LO, GO, NT>(total, base, comm));
RCP<Map<LO, GO, NT> > replicatedmap = rcp(new Map<LO, GO, NT>(total, total, base, comm));

RCP<matrix_type> contigmatrix = rcp(new matrix_type(contigmap, ents_per_row));
// fill in the matrix to make it a unit matrix

RCP<vector_type> replicated_vec = rcp(new vector_type(replicatedmap));
// fill in the vector to whatever values

// vector to store our matrix vector multiply
RCP<vector_type> contig_results_vec = rcp(new vector_type(contigmap));

contigmatrix->apply(*replicated_vec, *contig_results_vec);
// what's in contig_results_vec now?
```

Turns out we don't get anything meaningful but a result is provided nonetheless. 
This highlights that to be absolutely certain that our operations provide useful 
information, we need to have sensible Maps. Maps are compatible when

* they have the same number of global entries
* MPI processes in the Map's communicator that have the same MPI rank own the 
same number of entries

but if the Maps have different vector element ordering, summing two vectors for 
example may not actually make sense. Maps are the same when

* the minimum and maximum global indices are the same
* they have the same global number of entries
* the maps are both distributed over multiple processes or both not distributed
* they have the same index base
* processes that have the same rank own the same number of entries
* processes that have the same rank own the same entries in the same order

These requirements often are implicit when we build our objects so I wouldn't 
worry about commiting these rules to memory but maybe refer back to the 
[Tpetra Map Lesson](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/Tpetra_Lesson02.html) 
at the "Map compatibility" section or here if there's a bug that you suspect might 
have to do with the distribution of your objects. And in that last code snippet 
I gave, I also demonstrated the simplest use case of a matrix-vector multiply too 
which answers the first question I posed. The full definition of `apply()` is 
more involved and can be found [here](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/classTpetra_1_1CrsMatrix.html#a63ab2d56f57e555e3435f45157bffe7f).
