Now that we're able to create Tpetra objects, we can find out how to fill them 
out and print elements in them, which will involve Kokkos objects. Here's the 
example code from the previos section with a single added line at the top.

```cpp
#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>

typedef Tpetra::Vector<>::node_type NT;

int main(int argc, char* argv[])
{
  using Tpetra::Map;
  using Tpetra::Vector;
  using Tpetra::MultiVector;
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef int LO;
  typedef int GO;
  typedef double Scalar;

  const GO total = 500; // total number of elements in our vector
  const GO base = 0; // index base
  const int numvecs = 1; // we're only dealing with a single vector here

  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm(); // MPI communicator
    RCP<Map<LO, GO, NT> > contigmap = rcp(new Tpetra::Map<LO, GO, NT>(total, base, comm));

    /*
      Here we have a single vector split evenly and contiguously across our 
      process pool. The zeroOut option is true by default, I've just been 
      explicit in writing it out here.
    */
    RCP<Vector<Scalar, LO, GO, NT> > v = 
      rcp(new Tpetra::Vector<Scalar, LO, GO, NT>(contigmap, zeroOut=true));

    /*
      Here we have a multivector split contiguously like above. This does 
      exactly the same thing as the line above.
    */
    RCP<MultiVector<Scalar, LO, GO, NT> > mv =
      rcp(new Tpetra::MultiVector<Scalar, LO, GO, NT>(contigmap, numvecs, zeroOut=true));

    /* At this point we have two zeroed out vectors to play with. */
  }

  return 0;
}
```

The two simplest things we can do are:

* make all entries the same value via the `putScalar(const T &value)` method
* make all entries random via the `randomize()` or 
`randomize(const Scalar &minVal, const Scalar &maxVal)` methods

Next we can assign specific values to specific indices. This is more involved so 
I will demonstrate via an annotated example.

```cpp
  ...
  ...

  /* At this point we have two zeroed out vectors to play with, v and mv. */

  const GO row = 2; // we want to change the 3rd element in our vector
  const Scalar val = 1; // change the value to 1
  
  // check whether we own the global element locally
  if (contigmap->isNodeGlobalElement(row))
  {
    v->replaceGlobalValue(row, val); // replace for Vector
    v->replaceGlobalValue(row, 0, val); // replace for MultiVector
  }

  ...
  ...
```

There's a few things to note here already. Firstly, our typedefs at the start 
serve to both shorten our lines AND to allow our types to be modified at will 
later without having to tediously search and replace everything, such as if 
we realised we actually need to use complex doubles instead of double for the 
Scalar type. Secondly, global ordinal and local ordinal types may not be the 
same and so we need to use a Map method to check whether our local process 
owns a specific global index via `isNodeGlobalElement(GO g)`. And thirdly, 
when we update elements, we need to make sure we're updating the objects we 
locally own via a check. Funnily enough, you can call `replaceGlobalValue` with 
a global row you don't own and no error will be thrown but also nothing will 
happen! So this is not a point to really stress over but maybe you'll take a 
performance hit when you do a few million empty operations.

Other things to keep in the back of your head are that the `=` operator only 
does a shallow copy or move meaning if you want to modify 2 objects differently 
you need to look for a function that does a deep copy for you. For now I haven't 
done anything to do with differing host and execution spaces (eg. if you want to 
use GPUs) but there's another set of semantics to worry about should it come 
to that.

Let's look at filling the whole vector with different values now.

```cpp
  ...
  ...

  /* At this point we have two zeroed out vectors to play with, v and mv. */

  const GO row = 2; // we want to change the 3rd element in our vector
  const Scalar val = 1; // change the value to 1
  
  // check whether we own the global element locally
  if (contigmap->isNodeGlobalElement(row))
  {
    v->replaceGlobalValue(row, val); // replace for Vector
    v->replaceGlobalValue(row, 0, val); // replace for MultiVector
  }

  // sequential method
  for(LO i=0; i<contigmap->getMaxLocalIndex(); i++)
  {
    v->replaceLocalValue(i, i);
  }

  // via Kokkos parallelisation
  Kokkos::parallel_for("fill v", contigmap->getMaxLocalIndex(),
    KOKKOS_LAMBDA(const LO i)
      {
        mv->replaceLocalValue(i, 0, i);
      });

  ...
  ...
```

Here I used `replaceLocalValue` to fill in the vector. This is the same thing 
as `replaceGlobalValue` except now the task of making sure the row to change 
actually resides on the local process - if not an exception will be thrown.

In the next part of this section, I'm going to go through data extraction and 
various printing methods.
