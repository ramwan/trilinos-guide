What we should have now is the following code snippet.

```cpp
#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>

typedef Tpetra::Vector<>::node_type NT;

int main(int argc, char* argv[])
{
  using Tpetra::Map;
  using Tpetra::Vector;
  using Tpetra::MultiVector;
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef int LO;
  typedef int GO;
  typedef double Scalar;

  const GO total = 500; // total number of elements in our vector
  const GO base = 0; // index base
  const int numvecs = 1; // we're only dealing with a single vector here

  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm(); // MPI communicator
    RCP<Map<LO, GO, NT> > contigmap = rcp(new Tpetra::Map<LO, GO, NT>(total, base, comm));

    /*
      Here we have a single vector split evenly and contiguously across our 
      process pool. The zeroOut option is true by default, I've just been 
      explicit in writing it out here.
    */
    RCP<Vector<Scalar, LO, GO, NT> > v = 
      rcp(new Tpetra::Vector<Scalar, LO, GO, NT>(contigmap, zeroOut=true));

    /*
      Here we have a multivector split contiguously like above. This does 
      exactly the same thing as the line above.
    */
    RCP<MultiVector<Scalar, LO, GO, NT> > mv =
      rcp(new Tpetra::MultiVector<Scalar, LO, GO, NT>(contigmap, numvecs, zeroOut=true));

    /* At this point we have two zeroed out vectors to play with, v and mv. */

    const GO row = 2; // we want to change the 3rd element in our vector
    const Scalar val = 1; // change the value to 1
    
    // check whether we own the global element locally
    if (contigmap->isNodeGlobalElement(row))
    {
      v->replaceGlobalValue(row, val); // replace for Vector
      v->replaceGlobalValue(row, 0, val); // replace for MultiVector
    }

    // sequential method
    for(LO i=0; i<contigmap->getMaxLocalIndex(); i++)
    {
      v->replaceLocalValue(i, i);
    }

    // via Kokkos parallelisation
    Kokkos::parallel_for("fill v", contigmap->getMaxLocalIndex(),
      KOKKOS_LAMBDA(const LO i)
        {
          mv->replaceLocalValue(i, 0, i);
        });
  }

  return 0;
}
```

We have a few functions to get an array of our data with const and non-const 
options. The most basic for `Vector` are `getData()` and `getDataNonConst()`. 
These return `Teuchos::ArrayRCP<Scalar>` objects but the way to get around these 
annoying type names is to just do this:

```cpp
auto data = v->getData();
for(LO i=0; i<=contigmap->getMaxLocalIndex(); i++)
{
  std::cout << data[i] << std::endl;
}
```

Where here I've just used the `auto` keyword to not care about this temporary 
variable and just print it in order via a simple for loop. Note that if you 
print this in a program that's running with multiple processes, the order of 
output isn't exactly determined between processes but the order within a single 
process is determined. For example, 

```
...
0: 313
0: 314
0: 315
1: 247
1: 248
1: 249
...
```

Where the first number is the processor number and the second is the local row 
in a vector with 1000 entries.

For a MultiVector, we can also use `getData()` but we need to specify the column 
vector we want, such as `auto data = mv->getData(0);`. We can also use `get2dView()`
such as:

```cpp
auto view = mv->get2dView();
for(LO i=0; i<contigmap->getMaxLocalIndex(); i++)
{
  // because we only have 1 vector in the MultiVector object, the first index 
  // in our 2d view is 0.
  std::cout << view[0][i] << std::endl;
}
```

We can also get a 1 line description of our vector object via `description()` 
as well as print a Trilinos generated description of our object.

```cpp
RCP<std::basic_ostream<char> > stream = Teuchos::rcpFromRef(std::cout);
const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);
v->describe(*f_out /*, Teuchos::VERB_EXTREME */);
```

This method of printing is a bit more verbose but it gives a more nicely 
formatted output of your object such as:

```
"Tpetra::Vector":
Template parameters:
Scalar: double
LocalOrdinal: int
GlobalOrdinal: long long
Node: OpenMP/Wrapper
Global number of rows: 1000
Process 0 of 2:
Local number of rows: 500
Column stride: 500
Values:
[0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 32; 33; 34; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45; 46; 47; 48; 49; 50; 51; 52; 53; 54; 55; 56; 57; 58; 59; 60; 61; 62; 63; 64; 65; 66; 67; 68; 69; 70; 71; 72; 73; 74; 75; 76; 77; 78; 79; 80; 81; 82; 83; 84; 85; 86; 87; 88; 89; 90; 91; 92; 93; 94; 95; 96; 97; 98; 99; 100; 101; 102; 103; 104; 105; 106; 107; 108; 109; 110; 111; 112; 113; 114; 115; 116; 117; 118; 119; 120; 121; 122; 123; 124; 125; 126; 127; 128; 129; 130; 131; 132; 133; 134; 135; 136; 137; 138; 139; 140; 141; 142; 143; 144; 145; 146; 147; 148; 149; 150; 151; 152; 153; 154; 155; 156; 157; 158; 159; 160; 161; 162; 163; 164; 165; 166; 167; 168; 169; 170; 171; 172; 173; 174; 175; 176; 177; 178; 179; 180; 181; 182; 183; 184; 185; 186; 187; 188; 189; 190; 191; 192; 193; 194; 195; 196; 197; 198; 199; 200; 201; 202; 203; 204; 205; 206; 207; 208; 209; 210; 211; 212; 213; 214; 215; 216; 217; 218; 219; 220; 221; 222; 223; 224; 225; 226; 227; 228; 229; 230; 231; 232; 233; 234; 235; 236; 237; 238; 239; 240; 241; 242; 243; 244; 245; 246; 247; 248; 249; 250; 251; 252; 253; 254; 255; 256; 257; 258; 259; 260; 261; 262; 263; 264; 265; 266; 267; 268; 269; 270; 271; 272; 273; 274; 275; 276; 277; 278; 279; 280; 281; 282; 283; 284; 285; 286; 287; 288; 289; 290; 291; 292; 293; 294; 295; 296; 297; 298; 299; 300; 301; 302; 303; 304; 305; 306; 307; 308; 309; 310; 311; 312; 313; 314; 315; 316; 317; 318; 319; 320; 321; 322; 323; 324; 325; 326; 327; 328; 329; 330; 331; 332; 333; 334; 335; 336; 337; 338; 339; 340; 341; 342; 343; 344; 345; 346; 347; 348; 349; 350; 351; 352; 353; 354; 355; 356; 357; 358; 359; 360; 361; 362; 363; 364; 365; 366; 367; 368; 369; 370; 371; 372; 373; 374; 375; 376; 377; 378; 379; 380; 381; 382; 383; 384; 385; 386; 387; 388; 389; 390; 391; 392; 393; 394; 395; 396; 397; 398; 399; 400; 401; 402; 403; 404; 405; 406; 407; 408; 409; 410; 411; 412; 413; 414; 415; 416; 417; 418; 419; 420; 421; 422; 423; 424; 425; 426; 427; 428; 429; 430; 431; 432; 433; 434; 435; 436; 437; 438; 439; 440; 441; 442; 443; 444; 445; 446; 447; 448; 449; 450; 451; 452; 453; 454; 455; 456; 457; 458; 459; 460; 461; 462; 463; 464; 465; 466; 467; 468; 469; 470; 471; 472; 473; 474; 475; 476; 477; 478; 479; 480; 481; 482; 483; 484; 485; 486; 487; 488; 489; 490; 491; 492; 493; 494; 495; 496; 497; 498; 0]
Process 1 of 2:
Local number of rows: 500
Column stride: 500
Values:
[0; 1; 2; 3; 4; 5; 6; 7; 8; 9; 10; 11; 12; 13; 14; 15; 16; 17; 18; 19; 20; 21; 22; 23; 24; 25; 26; 27; 28; 29; 30; 31; 32; 33; 34; 35; 36; 37; 38; 39; 40; 41; 42; 43; 44; 45; 46; 47; 48; 49; 50; 51; 52; 53; 54; 55; 56; 57; 58; 59; 60; 61; 62; 63; 64; 65; 66; 67; 68; 69; 70; 71; 72; 73; 74; 75; 76; 77; 78; 79; 80; 81; 82; 83; 84; 85; 86; 87; 88; 89; 90; 91; 92; 93; 94; 95; 96; 97; 98; 99; 100; 101; 102; 103; 104; 105; 106; 107; 108; 109; 110; 111; 112; 113; 114; 115; 116; 117; 118; 119; 120; 121; 122; 123; 124; 125; 126; 127; 128; 129; 130; 131; 132; 133; 134; 135; 136; 137; 138; 139; 140; 141; 142; 143; 144; 145; 146; 147; 148; 149; 150; 151; 152; 153; 154; 155; 156; 157; 158; 159; 160; 161; 162; 163; 164; 165; 166; 167; 168; 169; 170; 171; 172; 173; 174; 175; 176; 177; 178; 179; 180; 181; 182; 183; 184; 185; 186; 187; 188; 189; 190; 191; 192; 193; 194; 195; 196; 197; 198; 199; 200; 201; 202; 203; 204; 205; 206; 207; 208; 209; 210; 211; 212; 213; 214; 215; 216; 217; 218; 219; 220; 221; 222; 223; 224; 225; 226; 227; 228; 229; 230; 231; 232; 233; 234; 235; 236; 237; 238; 239; 240; 241; 242; 243; 244; 245; 246; 247; 248; 249; 250; 251; 252; 253; 254; 255; 256; 257; 258; 259; 260; 261; 262; 263; 264; 265; 266; 267; 268; 269; 270; 271; 272; 273; 274; 275; 276; 277; 278; 279; 280; 281; 282; 283; 284; 285; 286; 287; 288; 289; 290; 291; 292; 293; 294; 295; 296; 297; 298; 299; 300; 301; 302; 303; 304; 305; 306; 307; 308; 309; 310; 311; 312; 313; 314; 315; 316; 317; 318; 319; 320; 321; 322; 323; 324; 325; 326; 327; 328; 329; 330; 331; 332; 333; 334; 335; 336; 337; 338; 339; 340; 341; 342; 343; 344; 345; 346; 347; 348; 349; 350; 351; 352; 353; 354; 355; 356; 357; 358; 359; 360; 361; 362; 363; 364; 365; 366; 367; 368; 369; 370; 371; 372; 373; 374; 375; 376; 377; 378; 379; 380; 381; 382; 383; 384; 385; 386; 387; 388; 389; 390; 391; 392; 393; 394; 395; 396; 397; 398; 399; 400; 401; 402; 403; 404; 405; 406; 407; 408; 409; 410; 411; 412; 413; 414; 415; 416; 417; 418; 419; 420; 421; 422; 423; 424; 425; 426; 427; 428; 429; 430; 431; 432; 433; 434; 435; 436; 437; 438; 439; 440; 441; 442; 443; 444; 445; 446; 447; 448; 449; 450; 451; 452; 453; 454; 455; 456; 457; 458; 459; 460; 461; 462; 463; 464; 465; 466; 467; 468; 469; 470; 471; 472; 473; 474; 475; 476; 477; 478; 479; 480; 481; 482; 483; 484; 485; 486; 487; 488; 489; 490; 491; 492; 493; 494; 495; 496; 497; 498; 0]
```
