The next Tpetra objects we're going to look at is Vector and MultiVector. The 
Vector class is a specialisation on MultiVector so the two are interchangeable 
barring a few method names such that Vector is a MultiVector with only 1 column.

```cpp
#include <Tpetra_Core.hpp>

typedef Tpetra::Vector<>::node_type NT;

int main(int argc, char* argv[])
{
  using Tpetra::Map;
  using Tpetra::Vector;
  using Tpetra::MultiVector;
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef int LO;
  typedef int GO;
  typedef double Scalar;

  const GO total = 500; // total number of elements in our vector
  const GO base = 0; // index base
  const int numvecs = 1; // we're only dealing with a single vector here

  Tpetra::ScopeGuard scope(&argc, &argv);
  {
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm(); // MPI communicator
    RCP<Map<LO, GO, NT> > contigmap = rcp(new Tpetra::Map<LO, GO, NT>(total, base, comm));

    /*
      Here we have a single vector split evenly and contiguously across our 
      process pool. The zeroOut option is true by default, I've just been 
      explicit in writing it out here.
    */
    RCP<Vector<Scalar, LO, GO, NT> > v =
      rcp(new Tpetra::Vector<Scalar, LO GO, NT>(contigmap, zeroOut=true));

    /*
      Here we have a multivector split contiguously like above. This does 
      exactly the same thing as the line above.
    */
    RCP<MultiVector<Scalar, LO, GO, NT> > mv =
      rcp(new Tpetra::MultiVector<Scalar, LO, GO, NT>(contigmap, numvecs, zeroOut=true));

    /* At this point we have two zeroed out vectors to play with. */
  }

  return 0;
}
```

Multivectors let us solve multiple linear systems at the same time and may 
improve the speed of some algorithms but for now it shouldn't matter too much. 
With the above few lines of code, we have made a vector and multivector object 
that is distributed over as many processes as we have in our process pool, with 
basically no extra effort on the programmer's part. Note that we told our Map 
object how we want our entries to be distributed and not the vector creation.


More notes on Vector and Multivector can be found in the docs:

* [Vector docs](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/classTpetra_1_1Vector.html#details)
* [MultiVector docs](https://docs.trilinos.org/latest-release/packages/tpetra/doc/html/classTpetra_1_1MultiVector.html#details)
