Annoyingly enough, the developers of Trilinos have found enough reason to have 
a more generic wrapper library around numerical software. This means that some 
packages don't want to deal with Tpetra objects directly but will want to deal 
with Thyra wrapped Tpetra objects. Let's see what this looks like in code:

```cpp
#include <iostream>
#include <Tpetra_Core.hpp>
#include <Kokkos_Core.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>

int main(int argc, char* argv[])
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  typedef Tpetra::Vector<>::scalar_type Scalar;
  typedef Tpetra::Vector<>::local_ordinal_type LO;
  typedef Tpetra::Vector<>::global_ordinal_type GO;
  typedef Tpetra::Vector<>::node_type NT;
  typedef Tpetra::Vector<> vector_type;
  typedef Tpetra::Map<LO, GO, NT> map_type;

  const GO total = 10;
  const GO base = 0;

  Tpetra::ScopeGuard tpetraScope(&argc, &argv);
  {
    RCP<const Teuchos::Comm<int> > comm = Tpetra::getDefaultComm();
    RCP<map_type> contigmap = rcp(new map_type(total, base, comm));

    RCP<const Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> > vbase =
      Thyra::tpetraVectorSpace<Scalar, LO, GO, NT>(contigmap);

    RCP<vector_type> v = rcp(new vector_type(contigmap));
    RCP<Thyra::TpetraVectorBase<Scalar, LO, GO, NT> > thyra_v =
      Thyra::tpetraVector<Scalar, LO, GO, NT>(v);

    // if we want to get our Tpetra vector out from the thyra vector
    RCP<vector_type> embedded_vec = thyra_v->getTpetraVector();

    std::ostream &out = std::cout;
    RCP<std::basic_ostream<char> > stream = Teuchos::rcpFromRef(out);
    const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);
    
    // this should print a 0 vector
    embedded_vec->describe(*f_out, Teuchos::VERB_EXTREME);
    embedded_vec->putScalar(2.0);
    // we can free this object now and it'll still be fine
    embedded_vec = Teuchos::null;

    RCP<vector_type> embedded_vec2 = thyra_v->getTpetraVector();
    embedded_vec2->describe(*f_out, Teuchos::VERB_EXTREME);
    // what does this now print?
  }

  return 0;
}
```

The final print statement in the above code will now print a vector filled with 
2s. 
