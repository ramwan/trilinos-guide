Now I'm going to combine Tpetra, Thyra, Kokkos and introduce a small bit of 
Stratimikos to demonstrate a linear solver. We only really need to use a small 
bit of Stratimikos and Thyra as you'll see :). To recap, the role of each of the 
Trilinos packages are:

* Tpetra - underlying linear algebra library to let us create distributed vector 
and operator objects
* Thyra - Linear algebra library wrapper so any solvers we use can be decoupled 
from specific linear algebra libraries
* Kokkos - underlying memory management and parallelisation package
* Stratimikos - thin management library to simplify the selection of solvers
* Belos - the actual linear solver library we will be using
* NOX - the nonlinear solver library that will be demonstrated later
* Teuchos - utilities package in Trilinos, providing parameter lists, smart pointers,
arrays etc.

To begin, I'm going to block out what the main sections of the code will be.

```cpp
#include <Stratimikos_DefaultLinearSolverBuilder.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_LinearOpWithSolveFactoryHelpers.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Kokkos_Core.hpp>

typedef double Scalar;
typedef int LO;
typedef int GO;
typedef Tpetra::Vector<>::node_type NT;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::Vector<Scalar, LO, GO, NT> vector_type;
typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;
typedef Tpetra::Operator<Scalar, LO, GO, NT> op_type;

int main(int argc, char* argv[])
{
  using Teuchos::ArrayView;
  using Teuchos::ArrayRCP;
  using Teuchos::ParameterList;
  using Teuchos::parameterList;
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  using Teuchos::tuple;
  typedef Teuchos::parameterList::PrintOptions PLPrintOptions;

  Tpetra::ScopeGuard tpetraScope(&argc, &argv);
  {
    /* setup code */
    auto comm = Tpetra::getDefaultComm();

    Teuchos::oblackholestream blackhole;
    std::ostream& out = (comm->getRank() == 0 ? std::cout : blackhole);
    RCP<std::basic_ostream<char> > stream = rcpFromRef(out);
    const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);
    /* end setup code */

    // 1. create the parameters and pass it to Stratimikos to build a Thyra 
    //    linear solver factory.
    //
    // 2. Create a Thyra linear operator solver based on a forward operator (matrix)
    //
    // 3. Perform the solve with an initial vector, filling the result in a different
    //    vector.
    //
    // 4. Get the solve result from the solver.
  }
  return 0;
}
```

There's not many steps, but there will be a lot of lines of code. I'll try to 
remind you of what stage we're at as I show the code but if you feel lost, refer 
back to here to get the overall big picture again.

```cpp
// This block of code goes under section 1.
RCP<ParameterList> p = parameterList();
p->set("Linear Solver Type", "Belos");
ParameterList& belosList = p->sublist("Linear Solver Types").sublist("Belos");
belosList.set("Solver Type", "Pseudo Block GMRES")
  .set<int>("Maximum Iterations", 100);
belosList.sublist("SolverTypes").sublist("Pseudo Block GMRES")
  .set<int>("Num Blocks", 8);
belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
  .set<double>("Convergence Tolerance", 1e-5);
belosList.sublist("VerboseObject").set("Verbosity Level", "low");
p->set("Preconditioner Type", "None");

Stratimikos::DefaultLinearSolverBuilder linearSolverBuilder;
linearSolverBuilder.setParameterList(p);
```

Here are a bunch of options for picking a Belos solver. You can get a full list 
of parameters printed for you by doing `linearSolverBuilder.getValidParameters()->
print(out, PLPrintOptions().indent(2).showTypes(true).showDoc(true));` or you can 
view a list online [here](https://github.com/trilinos/Trilinos/blob/master/packages/stratimikos/example/simple_stratimikos_example.options.readable.out) where 
the relevant options here are under Belos->Pseudo Block GMRES. A thing to note 
here is that if you set the verbosity level to high or extreme, the program will 
crash (don't know why, maybe too much is being printed). The "num blocks" option 
affects the total number of iterations able to be performed. For example with the 
above snippet, the implicit number for "Maximum Restarts" is 20. If we set 
"Num Blocks" to 1, we will only be able to do 21 iterations even though we set 
"Maximum Iterations" to 100. If your system isn't converging and you're also not 
reaching the maximum number of iterations, then you might need to increase 
the "Maximum Restarts" or "Num Blocks" numbers.

Step 2 is the lenghthiest and involves creating the relevant Tpetra objects,
wrapping them in Thyra wrappers and setting up the solver. Generally this is 
how Thyra will be used - as some sort of thin wrapper layer which we don't handle 
too much.

```cpp
// This code goes under point 2.

/* Create the Tpetra objects to be used. Hopefully this will be familiar by now :)
   I will however show a different way of filling out matrices and demonstrate 
   how finicky the Teuchos::ArrayView class can be to set up. */
const GO total = 100000;
const GO base = 0;

RCP<map_type> map = rcp(new map_type(total, base, comm)); // contiguous map
// an array of all the global elements on this process.
ArrayView<const GO> myGlobalElements = map->getNodeElementList();
// number of entries in a specific row
ArrayRCP<size_t> numNz = Teuchos::arcp<size_t>(total);

// Fill out numNz. We're going to be building a 1d finite difference matrix just 
// as an example. We can use Kokkos::parallel_for here but a regular for loop 
// fill do.
for(auto i=0; i<total; i++)
{
  if(myGlobalElements[i] == 0 || myGlobalElements[i] == total-1)
    numNz[i] = 2;
  else
    numNz[i] = 3;
}

// To create an array view, we have to have a pointer to raw memory and the size
// of our memory block.
ArrayView<const size_t> numNzView =
  ArrayView<const size_t>(numNz.getRawPtr(), myGlobalElements.size());

// Now we actually create the matrix. This matrix takes a Teuchos::ArrayView 
// instead of a number as the second parameter so we can specify different 
// maximum number of elements per row.
RCP<matrix_type> A = rcp(new matrix_type(map, numNzView));
// It may be ok to be skipped but just to be clear and avoid potential 
// compiler grumbling.
const Scalar two = 2.0;
const Scalar negOne = -1.0;

// Each process in our MPI pool will do this loop with their own block of 
// indices. Probably not really necessary to Kokkos::parallel_for this loop but
// demonstrating how it can be used.
Kokkos::parallel_for("filling matrix", myGlobalElements.size(),
  KOKKOS_LAMBDA(const size_t i)
    {
      GO g = myGlobalElements[i];

      if(g == 0)
        A->insertGlobalValues(g, tuple(g, g+1), tuple(two, negOne));
      else if (g == total - 1)
        A->insertGlobalValues(g, tuple(g-1, g), tuple(negOne, two));
      else
        A->insertGlobalValues(g, tuple(g-1, g, g+1), tuple(negOne, two, negOne));
    });

// Don't forget this line!
A->fillComplete();

// Create the guess vector, initialize it to some small random values.
RCP<vector_type> x = rcp(new vector_type(map));
x->randomize();
x->scale(0.1);

// Create the RHS vector. Note: this won't actually contain the solution after 
// the solve - the x vector will.
RCP<vector_type> b = rcp(new vector_type(map));
```

Now that we've created the relevant Tpetra objects, time to wrap them in Thyra 
wrappers.

```cpp
// continuing on from the previous code block

// typedefs otherwise our lines would run on forever
typedef Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> vec_space;
typedef Thyra::TpetraLinearOp<Scalar, LO, GO, NT> lin_op;
typedef Thyra::TpetraVector<Scalar, LO, GO, NT> vec;

RCP<vec_space> thyra_space =
  Thyra::tpetraVectorSpace<Scalar, LO, GO, NT>(map);
RCP<lin_op> thyra_A =
  Thyra::tpetraLinearOp<Scalar, LO, GO, NT>(thyra_space, thyra_space, A);
RCP<vec> thyra_x =
  Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, x);
RCP<vec> thyra_b =
  Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, b);
```

The above code snippet is really the extent of what we need to deal with Thyra 
for a linear solver. The `tpetraVectorSpace` object kind of parallels what a 
vector space in maths would be (the Tpetra map provided tells us how many elements 
are in the vector space) along with the distribution of objects. The Thyra 
lin-op and vector objects also allow basically the same basic operations as Tpetra
... but just in a Thyra object...

In the next section, I'm going to finish off the rest of the code and show an 
example run of the program too.
