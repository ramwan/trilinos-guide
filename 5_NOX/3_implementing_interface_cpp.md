Now to actually writing the implementation of `TpetraEvaluator`. Let's get the 
easiest stuff out of the way first. In `interface.cpp`, we're going to have 

```cpp
#include "interface.hpp"

const GO total = 1000; // modify as you will
const GO base = 0;

Teuchos::RCP<TpetraEvaluator> tpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A)
{
  return Teuchos::rcp(new TpetraEvaluator(comm, map, A));
}

TpetraEvaluator::TpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A) : A_(A), comm_(comm)
{
  typedef Thyra::ModelEvaluatorBase MEB; // convenience typedef

  TEUCHOS_ASSERT(nonnull(comm_));

  tmp_ = rcp(new vector_type(map));

  xSpace_ = Thyra::createVectorSpace<Scalar, LO, GO, NT>(map);
  fSpace_ = xSpace_;

  MEB::InArgsSetup<Scalar> inArgs;
  inArgs.setModelEvalDescription("Sample nonlinear solve");
  inArgs.setSupports(MEB::IN_ARG_x);
  prototypeInArgs_ = inArgs;

  MEB::OutArgsSetup<Scalar> outArgs;
  outArgs.setModelEvalDescription("Sample nonlinear solve");
  outArgs.setSupports(MEB::OUT_ARG_f);
  outArgs.setSupports(MEB::OUT_ARG_W_op);
  prototypeOutArgs_ = outArgs;
}
```

In this codeblock, we assign values to variables we declared in `interface.hpp` 
first. Note how here we don't have an `extern` preface to the type but in our 
header file we do. We then write our nonmember constructor which really is just 
a wrapper around the member constructor which is the next thing we write. The 
notation after the function signature is a shorthand for assigning variables to 
data members  which saves us a few lines. Then we check whether `comm_` is null 
and if it's not we create our temp vector and our vector spaces just like we 
did in previous chapters. The full list for [InArgs options](https://docs.trilinos.org/latest-release/packages/thyra/doc/html/classThyra_1_1ModelEvaluatorBase.html#a156d30e081cbe668b0e82d6c2be4bb6e) 
and [OutArgs options](https://docs.trilinos.org/latest-release/packages/thyra/doc/html/classThyra_1_1ModelEvaluatorBase.html#a0dcb168274ca26954a0b3ff74bc185b2) 
can be found in the docs but we'll only need these few.

This next codeblock will be pretty self-explanatory and brief.

```cpp
Teuchos::RCP<const thyra_space_base>
TpetraEvaluator::get_x_space() const
{
  return xSpace_;
}

Teuchos::RCP<const thyra_space_base>
TpetraEvaluator::get_f_space() const
{
  return fSpace_;
}

Thyra::ModelEvaluatorBase::InArgs<Scalar>
TpetraEvaluator::createInArgs() const
{
  return prototypeInArgs_;
}

Thyra::ModelEvaluatorBase::OutArgs<Scalar>
TpetraEvaluator::createOutArgsImpl() const
{
  return prorotypeOutArgs_;
}
```

And now onto the residual calculation. Our sample problem of `f(x) = Ax - x = 0` 
is pretty straight-forward but it can get hairy if matrix and vector element 
manipulations need to be done.

```cpp
void TpetraEvaluator::evalModelImpl
(const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
 const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs) const
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  typedef Thyra::TpetraOperatorVectorExtraction<Scalar, LO, GO, NT> tpetra_extract;

  TEUCHOS_ASSERT(nonnull(inArgs.get_x()));

  const RCP<thyra_base> f_out = outArgs.get_f();
  RCP<thyra_base> x_in = Teuchos::rcp_const_cast<thyra_base>(inArgs.get_x());

  if(nonnull(f_out))
  {
    RCP<vector_type> f = tpetra_extract::getTpetraVector(f_out);
    RCP<vector_type> x = tpetra_extract::getTpetraVector(x_in);

    // calculate Ax - x = tmp
    A_->apply(*x, *tmp_); // tmp = Ax
    tmp_->update(-1.0, *x, 1.0); // tmp = tmp - x
    // tmp_ now holds our residual

    auto myGlobalElements = f->getMap()->getMyGlobalIndices();
    auto residualView = tmp_->getData();
    Kokkos::parallel_for("copy into f", myGlobalElements.size(),
      KOKKOS_LAMBDA(const size_t i)
        {
          f->replaceGlobalValue(myGlobalElements[i], residualView[i]);
        });
  }
}
```

Our f vector is `f(x)` which we need to get out of our model and fill and the x 
vector is our current guess. We do a bit of typecasting to get everything to fit 
and eventually end up with Tpetra vectors which we can actually work with. Our 
function then has to be done in 2 steps and our result then ends up in `tmp_` 
and then we copy each element from `tmp_` into `f` using Kokkos and the vector 
filling method I introduced in the linear solver code. And that's it! That's the 
core of our nonlinear solver, at least what we need to write. NOX then takes 
the `f` vector that we filled out and does the heavy lifting.
