NOX exists to solve equations of the form F(x) = 0 with a variety of algorithms 
and linear algebra libraries. To use Tpetra with NOX, we need to use the Thyra 
wrappers we've come to know and love. Linking code with NOX will take a fair 
amount of extra work and will involve writing an implementation of a Thyra class 
specific to the problem you're trying to solve (ModelEvaluator).

The full documentation for the Thyra ModelEvaluator class can be found [here](https://docs.trilinos.org/latest-release/packages/thyra/doc/html/classThyra_1_1ModelEvaluator.html#details). 
As an overview, the interface defines a way to evaluate a stateless model that 
can represent different problems. For example, the nonlinear poisson solver code 
that I've been working on falls under the "state vector function" category, 
solving a problem of the form `(x_dot_dot, x_dot, x, {p(1), t,...}) -> f <: f_space` 
where the left side are our function inputs with extra parameters. The docs are 
pretty detailed but I've only handled a small section of this class so I will just 
stick to what I know here.

[Here](https://docs.trilinos.org/latest-release/packages/thyra/doc/html/group__Thyra__Nonlin__ME__support__grp.html) 
is where you can find a list of the nonlinear model evaluator classes that Thyra 
offers but the one we'll be implementing is at the bottom - `StateFuncModelEvaluatorBase<Scalar>` 
with direct docs link [here](https://docs.trilinos.org/latest-release/packages/thyra/doc/html/classThyra_1_1StateFuncModelEvaluatorBase.html). 
The only functions we have to implement are `get_x_space()`, `get_f_space()`, 
`createInArgs()`, `createOutArgsImpl()` and `evalModelImpl()` where `evalModelImpl()` 
is where we actually do residual calculations and vector filling and the works.

If you have a look into the `code/` directory, you should see this setup:

```
code
 |
 +--> main.cpp
 +--> interface.hpp // definitions of our model evaluator class
 +--> interface.cpp // implementation of our model evaluator class
 +--> CMakeLists.txt
 +--> output.out // what you should see after your program has been completed
```

To run the code immediately, just navigate into this directory and run the 
commands `cmake CMakeLists.txt` and `make`. 
