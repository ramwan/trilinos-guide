I'm going to first introduce you to the Thyra::ModelEvaluator class that we 
need to make an implementation of. In the file where this class is declared, I'm 
also going to put our typedefs and header file includes because this file is 
common to both `main.cpp` and  `interface.cpp`.

```cpp
#ifndef _INTERFACE_
#define _INTERFACE_

#include <Kokkos_Core.hpp>
#include <Stratimikos_DefaultLinearSolverBuilder.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_LinearOpWithSolveFactoryHelpers.hpp>
#include <Thyra_StateFuncModelEvaluatorBase.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>

#include <NOX.H>
#include <NOX_MatrixFree_ModelEvaluatorDecorator.hpp>
#include <NOX_Thyra.H>
#include <NOX_Thyra_MatrixFreeJacobianOperator.hpp>

typedef double Scalar;
typedef int LO;
typedef int GO;
typedef Tpetra::Vector<>::node_type NT;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::Vector<Scalar, LO, GO, NT> vector_type;
typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;
typedef Tpetra::Operator<Scalar, LO, GO, NT> op_type;

typedef Thyra::VectorBase<Scalar> thyra_base;
typedef Thyra::VectorSpaceBase<Scalar> thyra_space_base;
typedef Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> thyra_space;
typedef Thyra::TpetraLinearOp<Scalar, LO, GO, NT> thyra_op;
typedef Thyra::TpetraVector<Scalar, LO, GO, NT> thyra_vec;

typedef Teuchos::ParameterList::PrintOptions PLPrintOptions;

extern const GO total;
extern const GO base;

class TpetraEvaluator;

// Nonmember constructor
Teuchos::RCP<TpetraEvaluator> tpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A);
```

Here are all the definitions and includes I have. The first and second lines 
are called include guards, making sure that your header file is only included 
once and no compiler errors occur if multiple files include the same header file. 
At the very bottom of the file, we will have an `#endif`. I also declare `total` 
and `base` (total number of vector elements and base index of our vectors) with 
the `extern` statement meaning our value is actually set elsewhere (in this case, 
set in `interface.cpp`. Then I declare the class `TpetraEvaluator` which is the 
meat of the nonlinear solver and a nonmember constructor. We don't really need 
this but Thyra likes to use them so I've just included it anyway.

```cpp
// Class extension
class TpetraEvaluator : public Thyra::StateFuncModelEvaluatorBase<Scalar>
{
public:
  TpetraEvaluator(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
                  Teuchos::RCP<const map_type> map,
                  Teuchos::RCP<const matrix_type> A);

  Teuchos::RCP<const thyra_space_base > get_x_space() const;
  Teuchos::RCP<const thyra_space_base > get_f_space() const;

  Thyra::ModelEvaluatorBase::InArgs<Scalar> createInArgs() const;

private:
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> createOutArgsImpl() const;
  void evalModelImpl(const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
                     const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs
                    )const;
```

Here is all we really need to implement from the `StateFuncModelEvaluatorBase` 
class - thankfully it's not a lot. Most of the code will exist in the 
`evalModelImpl` function where we actually need to calculate our residual. 
After these function declarations, we then need to put  in the class variables 
we want to use. This will be problem specific. The sample problem I'm going to 
solve here is `f(x) = Ax - x = 0` so we'll need our class to keep track of our 
matrix `A` along with a few other objects needed for evaluation. We don't need 
to keep track of our guess vector `x` explicitly as the framework we're using 
does it for us.

```cpp
// this goes right under the above codeblock
private: // data members
  const Teuchos::RCP<const Teuchos::Comm<int> > comm_;
  Teuchos::RCP<const thyra_space_base> xSpace_;
  Teuchos::RCP<const thyra_space_base> fSpace_;

  Thyra::ModelEvaluatorBase::InArgs<Scalar> prototypeInArgs_;
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> prototypeOutArgs_;

  Teuchos::RCP<const matrix_type> A_;
  Teuchos::RCP<vector_type> tmp_;
}; // end of class TpetraEvaluator

#endif // here's the #endif i mentioned earlier :)
```

All our data members are private (if you're uncomfortable with public, private 
and protected, feel free to stop and have a google). CPP has a convention where 
private data members have a trailing `_` for ease of programming so I've also 
done that here. `xSpace_` and `fSpace_` should be the same as our objects all 
live in the same vector space. We then have an `InArgs` object which sets what 
inputs we can give to the function (in our case all we'll input is a vector) and 
and `OutArgs` object which sets what objects we can calculate. In our case, we'll 
be calculating f(x) and the Jacobian operator. Our class also has our matrix and 
a temporary vector to be used in residual calculations.

And that brings us to the end of `interface.hpp`!
