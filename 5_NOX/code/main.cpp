#include "interface.hpp"

int main(int argc, char *argv[])
{
  using Teuchos::ArrayView;
  using Teuchos::ArrayRCP;
  using Teuchos::null;
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  using Teuchos::tuple;
  
  Tpetra::ScopeGuard tpetracope(&argc, &argv);
  {
    auto comm = Tpetra::getDefaultComm();
    Teuchos::oblackholestream blackhole;
    std::ostream& out = (comm->getRank() == 0 ? std::cout : blackhole);
    RCP<std::basic_ostream<char> > stream = rcpFromRef(out);
    const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);

    /* Create the Tpetra objects to be used */
    RCP<map_type> map = rcp(new map_type(total, base, comm)); // contiguous map
    // an array of all the global elements on this process
    ArrayView<const GO> myGlobalElements = map->getNodeElementList();
    // number of entries in a specific row
    ArrayRCP<size_t> numNz = Teuchos::arcp<size_t>(total);

    // to be Kokkos-ified
    for(auto i=0; i<total; i++)
    {
      if(myGlobalElements[i] == 0 || myGlobalElements[i] == total-1)
        numNz[i] = 2;
      else
        numNz[i] = 3;
    }

    ArrayView<const size_t> numNzView =
      ArrayView<const size_t>(numNz.getRawPtr(), myGlobalElements.size());
    // create the finite difference matrix
    RCP<matrix_type> A = rcp(new matrix_type(map, numNzView));
    const Scalar two = 2.0;
    const Scalar negOne = -1.0;

    // to be Kokkos-ified
    for(auto i=0; i<total; i++)
    {
      if(myGlobalElements[i] == 0)
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i], myGlobalElements[i]+1),
                              tuple(two, negOne));
      else if (myGlobalElements[i] == total - 1)
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i]-1, myGlobalElements[i]),
                              tuple(negOne, two));
      else
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i]-1, myGlobalElements[i], myGlobalElements[i]+1),
                              tuple(negOne, two, negOne));
    }
    A->fillComplete();

    // create the vectors
    RCP<vector_type> b = rcp(new vector_type(map));
    RCP<vector_type> x = rcp(new vector_type(map));
    b->putScalar(0.1);
    x->putScalar(0.1);

    /* Thyra code to link Tpetra with our solver of choice */
    typedef Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> vec_space;
    typedef Thyra::TpetraLinearOp<Scalar, LO, GO, NT> lin_op;
    typedef Thyra::TpetraVector<Scalar, LO, GO, NT> vec;

    RCP<vec_space> thyra_space =
      Thyra::tpetraVectorSpace<Scalar, LO, GO, NT>(map);
    RCP<lin_op> thyra_A =
      Thyra::tpetraLinearOp<Scalar, LO, GO, NT>(thyra_space, thyra_space, A);
    RCP<vec> thyra_x =
      Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, x);
    RCP<vec> thyra_b =
      Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, b);

    Stratimikos::DefaultLinearSolverBuilder linearSolverBuilder;

    RCP<Teuchos::ParameterList> p = Teuchos::parameterList();
    p->set("Linear Solver Type", "Belos");
    Teuchos::ParameterList& belosList =
      p->sublist("Linear Solver Types").sublist("Belos");
    belosList.set("Solver Type", "Pseudo Block GMRES");
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Maximum Iterations", 300);
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Num Blocks", 8);
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<double>("Convergence Tolerance", 1e-8);
    belosList.sublist("VerboseObject").set("Verbosity Level", "default");
    p->set("Preconditioner Type", "None");
    linearSolverBuilder.setParameterList(p);

    typedef Thyra::LinearOpWithSolveFactoryBase<Scalar> opFactory;
    RCP<opFactory> lowsFactory = 
      linearSolverBuilder.createLinearSolveStrategy("");
    lowsFactory->setOStream(f_out);
    lowsFactory->setVerbLevel(Teuchos::VERB_LOW);

    // Jacobian-free operator
    Teuchos::ParameterList printParams;
    RCP<Teuchos::ParameterList> jfnkParams = Teuchos::parameterList();
    jfnkParams->set("Difference Type", "Forward");
    jfnkParams->set("Perturbation Algorithm", "KSP NOX 2001");
    jfnkParams->set("lambda", 1.0e-4);
    RCP<NOX::Thyra::MatrixFreeJacobianOperator<Scalar> > jfnkOp =
      rcp(new NOX::Thyra::MatrixFreeJacobianOperator<Scalar>(printParams));
    jfnkOp->setParameterList(jfnkParams);
    jfnkParams->print(out);

    // Our implementation of Thyra::ModelEvaluatorBase
    RCP<TpetraEvaluator> evaluator = tpetraEvaluator(comm, map, A);

    RCP<Thyra::ModelEvaluator<Scalar> > thyraModel =
      rcp(new NOX::MatrixFreeModelEvaluatorDecorator<Scalar>(evaluator));

    // Create our NOX group
    RCP<NOX::Thyra::Group> nox_group =
      rcp(new NOX::Thyra::Group(*thyra_x, evaluator, jfnkOp, 
                                lowsFactory, null, null, null));
    nox_group->computeF(); // why do we need this line?

    // VERY IMPORTANT apparently
    // the JFNK object needs base evaluation objects. This creates a circular 
    // dependency so use a weak pointer.
    jfnkOp->setBaseEvaluationToNOXGroup(nox_group.create_weak());

    // NOX status tests
    RCP<NOX::StatusTest::NormF> absresid =
      rcp(new NOX::StatusTest::NormF(1.0e-6));
    RCP<NOX::StatusTest::NormWRMS> wrms =
      rcp(new NOX::StatusTest::NormWRMS(1.0e-2, 1.0e-8));
    RCP<NOX::StatusTest::Combo> converged =
      rcp(new NOX::StatusTest::Combo(NOX::StatusTest::Combo::AND));
    converged->addStatusTest(absresid);
    converged->addStatusTest(wrms);
    RCP<NOX::StatusTest::MaxIters> maxiters =
      rcp(new NOX::StatusTest::MaxIters(300));
    RCP<NOX::StatusTest::FiniteValue> fv =
      rcp(new NOX::StatusTest::FiniteValue);
    RCP<NOX::StatusTest::Combo> combo =
      rcp(new NOX::StatusTest::Combo(NOX::StatusTest::Combo::OR));
    combo->addStatusTest(fv);
    combo->addStatusTest(converged);
    combo->addStatusTest(maxiters);

    // NOX parameter list
    RCP<Teuchos::ParameterList> nl_params = Teuchos::parameterList();
    nl_params->set("Nonlinear Solver", "Line Search Based");
    nl_params->sublist("Direction").sublist("Newton")
      .sublist("Linear Solver").set("Tolerance", 1.0e-8);

    // Set output parameters
    nl_params->sublist("Printing").sublist("Output Information").set("Debug", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Warning", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Error", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Test Details", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Details", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Parameters", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Linear Solver Details", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Inner Iteration", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Outer Iteration", true);
    nl_params->sublist("Printing").sublist("Output Information").set("Outer Iteration StatusTest", true);

    // Create the nonlinear solver
    RCP<NOX::Solver::Generic> solver =
      NOX::Solver::buildSolver(nox_group, combo, nl_params);
    NOX::StatusTest::StatusType solvStatus = solver->solve();

    // Get the results
    NOX::Thyra::Group finalGroup =
      dynamic_cast<const NOX::Thyra::Group&>( solver->getSolutionGroup() );
    RCP<const Thyra::VectorBase<Scalar> > solvecbase =
      dynamic_cast<const NOX::Thyra::Vector&>( finalGroup.getX() )
        .getThyraRCPVector();

    typedef Thyra::TpetraOperatorVectorExtraction<Scalar, LO, GO, NT> tpetra_extract;
    RCP<Thyra::VectorBase<Scalar> > v =
      Teuchos::rcp_const_cast<Thyra::VectorBase<Scalar> >(solvecbase);
    RCP<vector_type> solution = tpetra_extract::getTpetraVector(solvecbase);

    // Print the solve details
    out << solvStatus << std::endl;
    solution->describe(*f_out, Teuchos::VERB_EXTREME);
  }

  return 0;
}
