#include "interface.hpp"

const GO total = 1000;
const GO base = 0;

Teuchos::RCP<TpetraEvaluator> tpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A)
{
  return Teuchos::rcp(new TpetraEvaluator(comm, map, A));
}

TpetraEvaluator::TpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A) : A_(A), comm_(comm)
{
  typedef Thyra::ModelEvaluatorBase MEB;

  TEUCHOS_ASSERT(nonnull(comm_));

  tmp_ = rcp(new vector_type(map));

  xSpace_ = Thyra::createVectorSpace<Scalar, LO, GO, NT>(map);
  fSpace_ = xSpace_;

  MEB::InArgsSetup<Scalar> inArgs;
  inArgs.setModelEvalDescription("Sample nonlinear solve");
  inArgs.setSupports(MEB::IN_ARG_x);
  prototypeInArgs_ = inArgs;

  MEB::OutArgsSetup<Scalar> outArgs;
  outArgs.setModelEvalDescription("Sample nonlinear solve");
  outArgs.setSupports(MEB::OUT_ARG_f);
  outArgs.setSupports(MEB::OUT_ARG_W_op);
  prototypeOutArgs_ = outArgs;
}

Teuchos::RCP<const thyra_space_base>
TpetraEvaluator::get_x_space() const
{
  return xSpace_;
}

Teuchos::RCP<const thyra_space_base>
TpetraEvaluator::get_f_space() const
{
  return fSpace_;
}

Thyra::ModelEvaluatorBase::InArgs<Scalar>
TpetraEvaluator::createInArgs() const
{
  return prototypeInArgs_;
}

Thyra::ModelEvaluatorBase::OutArgs<Scalar>
TpetraEvaluator::createOutArgsImpl() const
{
  return prototypeOutArgs_;
}

void TpetraEvaluator::evalModelImpl
(const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
 const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs) const
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  typedef Thyra::TpetraOperatorVectorExtraction<Scalar, LO, GO, NT> tpetra_extract;

  TEUCHOS_ASSERT(nonnull(inArgs.get_x()));

  const RCP<thyra_base> f_out = outArgs.get_f();
  RCP<thyra_base> x_in = Teuchos::rcp_const_cast<thyra_base>( inArgs.get_x() );

  if (nonnull(f_out))
  {
    RCP<vector_type> f = tpetra_extract::getTpetraVector(f_out);
    RCP<vector_type> x = tpetra_extract::getTpetraVector(x_in);

    // calculate Ax - x = tmp
    A_->apply(*x, *tmp_); // tmp = Ax
    tmp_->update(-1.0, *x, 1.0); // tmp = tmp - x

    auto myGlobalElements = f->getMap()->getMyGlobalIndices();
    auto residualView = tmp_->getData();
    Kokkos::parallel_for("copy into f", myGlobalElements.size(),
      KOKKOS_LAMBDA(const size_t i)
        {
          f->replaceGlobalValue(myGlobalElements[i], residualView[i]);
        });
  }
}
