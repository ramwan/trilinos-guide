#ifndef _INTERFACE_
#define _INTERFACE_

#include <Kokkos_Core.hpp>
#include <Stratimikos_DefaultLinearSolverBuilder.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_LinearOpWithSolveFactoryHelpers.hpp>
#include <Thyra_StateFuncModelEvaluatorBase.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>

#include <NOX.H>
#include <NOX_MatrixFree_ModelEvaluatorDecorator.hpp>
#include <NOX_Thyra.H>
#include <NOX_Thyra_MatrixFreeJacobianOperator.hpp>

typedef double Scalar;
typedef int LO;
typedef int GO;
typedef Tpetra::Vector<>::node_type NT;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::Vector<Scalar, LO, GO, NT> vector_type;
typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;
typedef Tpetra::Operator<Scalar, LO, GO, NT> op_type;

typedef Thyra::VectorBase<Scalar> thyra_base;
typedef Thyra::VectorSpaceBase<Scalar> thyra_space_base;
typedef Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> thyra_space;
typedef Thyra::TpetraLinearOp<Scalar, LO, GO, NT> thyra_op;
typedef Thyra::TpetraVector<Scalar, LO, GO, NT> thyra_vec;

typedef Teuchos::ParameterList::PrintOptions PLPrintOptions;

extern const GO total;
extern const GO base;

class TpetraEvaluator;

// Nonmember constructor
Teuchos::RCP<TpetraEvaluator> tpetraEvaluator
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> map,
 Teuchos::RCP<const matrix_type> A);

// Class extension
class TpetraEvaluator : public Thyra::StateFuncModelEvaluatorBase<Scalar>
{
public:
  TpetraEvaluator(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
                  Teuchos::RCP<const map_type> map,
                  Teuchos::RCP<const matrix_type> A);

  Teuchos::RCP<const thyra_space_base > get_x_space() const;
  Teuchos::RCP<const thyra_space_base > get_f_space() const;

  Thyra::ModelEvaluatorBase::InArgs<Scalar> createInArgs() const;

private:
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> createOutArgsImpl() const;
  void evalModelImpl(const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
                     const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs
                    )const;
private: // data members
  const Teuchos::RCP<const Teuchos::Comm<int> > comm_;
  Teuchos::RCP<const thyra_space_base > xSpace_;
  Teuchos::RCP<const thyra_space_base > fSpace_;

  Thyra::ModelEvaluatorBase::InArgs<Scalar> prototypeInArgs_;
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> prototypeOutArgs_;

  Teuchos::RCP<const matrix_type> A_;
  Teuchos::RCP<vector_type> tmp_;
};

#endif
