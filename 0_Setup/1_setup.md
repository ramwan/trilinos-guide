In order to be able to compile and link with Trilinos, we need to set up our 
directory so that gcc knows where to look. Trilinos recommends the use of Cmake 
in order to manage the building of your program so I've copied the Cmake files 
that I've used here so you can also just copy and paste them wherever you want.

The file called CMakeLists.txt should reside in the root of your project 
directory. You'll need to change lines 8, 42, 43 and 68 by replacing the dots. 

* Line 8 needs where your trilinos installation is. For example, I had 
/home/561/rw6126/trilinos/13.0/
* Line 42 needs the name of your project
* Line 43 needs the root of your project. For example, I had
/scratch/ad73/rw6126/trilinos_tutorials as the root and all the code lived there.
* Line 68 needs the location you want to put your compiled binaries. This could 
just be ${PROJECT_SOURCE_DIR}/bin} which will put all your compiled binaries 
in a folder called bin.

At the very bottom, you can enable testing if you want to get cmake to run tests 
for you as well as put all your ADD_SUBDIRECTORY() commands. This lets you 
split your code into different locations but have cmake compile it all together 
with no hassle.

The file called "subdirectory_cmakelists.txt" is an example CMakeLists which you 
can copy into a subdirectory (just make sure to then rename it to CMakeLists.txt). 
The commented out lines and dots are what you'll need to replace with your own 
file names. Here's how I've been using the various functions (the online docs 
for cmake give the full usages).

```
* ADD_LIBRARY(object_A OBJECT object_A.cpp) -  create an object called object_A 
from object_A.cpp without outputing a library to a directory.
* ADD_EXECUTABLE(exe
                 main.cpp
                 $<TARGET_OBJECTS:object_A>) - create an executable called exe 
                                               compiled from main and linked 
                                               with object_A, linking against 
                                               Trilinos if need be.
```

The first time you run cmake, you should run `cmake CMakeLists.txt` in the root 
of your project. When you want to build your project, you can run `make` and it 
doesn't have to be in the root directory. If you run make in a subdirectory, 
it'll make that subdirectory's objects and any other subdirectories relevant.

As a summary, here's what a project tree would look like following this simple 
example.

```
/scratch/ad73/project_home/
      |
      |
      +--> CMakeLists.txt
      |
      +--> subdirectoryA/
                |
                |
                +--> CMakeLists.txt
```

Note: in my CMakeLists, I've also included the gsl library. If you don't want it, 
feel free to remove the `SET(CMAKE_EXE_LINKER_FLAGS "-lgsl")` line or you can 
do `module load gsl` to be able to compile with gsl.
