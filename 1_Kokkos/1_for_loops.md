To start off with, here's the [Kokkos wiki page](https://github.com/kokkos/kokkos/wiki)
which is really helpful and has a lot of explanations in understandable language.

As a preface to all the code snippets, I'm implying that I've put the line
`#include <Kokkos_Core.hpp>` at the top of whatever file I'm working on.


The first thing that we might want to parallelise is a long running for loop.
Kokkos gives us 3 different kinds of parallelised looping functions and then
lets us specify how the looping within it actually behaves. These are:

- parallel\_for
- parallel\_reduce
- parallel\_scan

The parallel\_for loop is a regular for loop with independent iterations.
You can use all of the loops with lambdas or functors, but for simplicity I've
been using lambdas everywhere. Here's an example of a parallel for.

```cpp
using Kokkos::parallel_for;

const int valueToSet = 0;
const int size = 10000;
int someArray[size];

Kokkos::RangePolicy<> rangePolicy(0, size);
parallel_for("for loop name", rangePolicy,
  KOKKOS_LAMBDA(const int i)
  {
    someArray[i] = valueToSet;
  });
```

The above block is equivalent to doing the following:

```cpp
const int valueToSet = 0;
const int size = 10000;
int someArray[size];

for(auto i=0; i<size; ++i)
  someArray[i] = valueToSet;
```

The RangePolicy object in the Kokkos sample above says go from 0 to size-1, 
which is the behaviour of our non-Kokkos sample. In fact, if we wanted to we 
could just replace the RangePolicy object in such a simple example with the
integer representing the size of our object, "size".

What if we had a 2d array to iterate through though? For example in the next
code snippet.

```cpp
const int isize = 100;
const int jsize = 100;
int someArray[isize][jsize];

for(auto i=0; i<isize; ++i)
{
  for(auto j=0; j<jsize; ++j)
    someArray[i][j] = 0;
}
```

The Kokkos answer is a different range policy called MDRangePolicy.

```cpp
const int isize = 100;
const int jsize = 100;
int someArray[isize][jsize];
Kokkos::MDRangePolicy<Kokkos::Rank<2> > policy({0, 0}, {isize, jsize});

Kokkos::parallel_for("loop name", policy,
  KOKKOS_LAMBDA(int i, int j)
  {
    someArray[i][j] = 0;
  });
```

Reduction and scan loops have their own uses which I haven't really encountered
when doing the nonlinear Poisson solver but the parallel_for has been heavily
used to fill in matrices and vectors which I will get to soon.
