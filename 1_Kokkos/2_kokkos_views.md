Trilinos packages make a distinction between an object and actually accessing
it to get what's inside which is different to a regular cpp vector or python
arrays. Here's a comparison of what I mean in python, normal cpp and Trilinos.

```python
# Python example
someArray = [1, 2, 3]

if someArray[0] == 1:
  pass # do something
```

```cpp
// cpp example using vectors
#include <iostream>
#include <vector>

int main()
{
  std::vector<int> v = {1, 2, 3};
  if(v[0] == 1) {/* do something */}
  return 0;
}
```

```cpp
// cpp example with Trilinos
#include <Kokkos_Core.hpp>
#include <Tpetra_Core.hpp>

int main(int argc, char* argv[])
{
  Tpetra::ScopeGuard tpetraScope(&argc, &argv);
  {
    Tpetra::Vector<> v = Tpetra::Vector<>(); // abbreviated creation to get the point across
    auto viewConst = v.get1dView(); // constant vector View
    for(...; ...; ...;) {/* do stuff with the values */}

    auto viewNonConst = v.get1dViewNonConst(); // non-const vector View
    for(...; ...; ...;) {/* we can change the vector here */}
  }
  return 0;
}
```

As a note, the second cpp code snippet above will not run - I shortened the lines
as Trilinos can get quite wordy. Otherwise, they all just create an array or
vector object and let us play with it. Kokkos views are meant to do all the work 
of figuring out how to lay data in memory so we can focus on other stuff. 
However if you feel so inclined, you can actually change the data layout of Views 
which will greatly affect runtimes (this [Kokkos exercise](https://github.com/kokkos/kokkos-tutorials/blob/main/Exercises/04/Begin/exercise_4_begin.cpp) 
demonstrates it). Although I haven't actually needed to create and work with 
Views a lot, the basics are needed to know how to access linear algebra objects 
and what the documentation in other Trilinos packages are saying.

A Kokkos View can have dimensions specified at both run-time or compile-time, 
but run-time dimensions need to be specified first in the type. What this means 
is:

```cpp
// Kokkos #includes and other setup stuff
const int dim1 = ...; // run-time determined, eg. read in from user input
const int dim2 = 2; // compile-time determined
Kokkos::View<int*[dim2]> v("debugging name", dim1);
```

In the above example, we made a View called 'v' of dimensions dim1x2 where dim1 
was specified at run-time, maybe computed from a database or given by a user. 
Note that dim1 (specified in the constructor argument) corresponds with the * 
type in the template type declaration (the <> brackets) and comes first. If we 
did `Kokkos::View<int[dim2]*>` instead, it would be invalid for reasons stemming 
from how cpp implements templates.

To access elements in a Kokkos View, we use () instead of the regular [], such 
as `v(i, j, k)`.
