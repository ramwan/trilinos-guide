Templates are a tool in cpp that let us write a single function that can work 
for a variety of different types without having to overload the function for 
all the types that we want it to work for. Templates need to be declared in 
header files, so the standard way to write both the function/class declarations 
and template definitions are to put the actual declarations in "file.hpp" and 
the template functions in "file_def.hpp".

For example,

```cpp
// file.hpp
template<class T>
void somefunc(T t);

// file_def.hpp
template<class T>
void somefunc(T t)
{
  // actual function body
}
```
