Here I'm going to describe the factory design pattern which is used in a whole 
bunch of Trilinos packages. Unless we're going to be writing large user programs 
we probably won't need to be implementing this pattern ourselves but it might 
help to understand what it is and how to use it.

Suppose we have an app for a company that delivers goods via road. A very 
simplistic approach could be to have a class called `Truck` that holds all the 
logic associated with managing road deliveries. However if we wanted to expand 
this app for other companies that want to deliver via sea or air, as it stands 
we'd have to copy and paste a whole lot of logic into classes called `Plane` and 
`Ship`. This greatly increases management and overhead for when bugs are 
discovered or if new functionality needs to be added across all the classes. 
The client code or user will also need to explicitly know the names of all the 
classes and how to use them.

We could very easily (albeit with some effort at the start) decouple the 
creation of our delivery classes and their logic to what the user code or client 
really wants to care about. To make this clear,

```
      [ Delivery ] // the object that the client will actually use
            ^
            |
            |
            |
            |
  ----------------------
  |         |          |
Truck     Ship       Plane   // Objects with actual code implementations


// Where Truck, Ship and Plane inherit from a base class.
```

For example in Trilinos, to use Belos, we could learn all about Belos, Thyra, 
Nox and try find the relevant classes via the documentation or we could do:

```cpp
Stratimikos::DefaultLinearSolverBuilder builder;

Teuchos::RCP<Teuchos::ParameterList> p = Teuchos::parameterList();
/* fill in relevant parameters which I will go into detail later */

Teuchos::RCP<Thyra::LinearOpWithSolveFactoryBase<Scalar> >
  lowsFactory = builder.createLinearSolveStrategy("");

Teuchos::RCP<NOX::Thyra::Group> nox_group =
  Teuchos::rcp(new NOX::Thyra::Group(/*parameters*/, lowsFactory, /* more params */);
```

The above already looks highly convoluted (which it is, exacerbated by the sheer 
number of options Trilinos has) but this is the Trilinos preferred way of 
calling its objects. 

The alternative to the above cpp pseudocode would be to find exactly which 
linear_op_with_solve object you want. 
