In part 4 of this chapter, I used the Teuchos::ParameterList object without 
explaining what it is and how it works. The ParameterList object lets you make 
a map from strings to values and can be hierarchically nested. Most commonly this 
object is used to give parameters to factory object creators.

```cpp
#include <Teuchos_ParameterList.hpp>
#include <Teuchos_RCPDecl.hpp>

int main()
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::ParameterList;

  p->set("main name", "main value");
  Teuchos::ParameterList& sublist = p->sublist("main name").sublist("sublist name");
  sublist.set<int>("sublist name", 1);

  /* and so on and so on...
     Here we have a list that looks like

     "main name" --> "main value"
            |
            |------> "sublist name" --> 1
  */

  return 0;
}
```
