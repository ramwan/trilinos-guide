#include <iostream>

int main()
{
  int a = 1;
  const int *b = &a;
  std::cout << *b << std::endl; // 1

  a = 2;
  std::cout << *b << std::endl; // 2
  // *b = 3; // this gives an error
  *const_cast<int*>(b) = 3;
  std::cout << a << std::endl; // 3
  std::cout << *b << std::endl; // 3

  const int c = 1;
  const int *d = &c;
  std::cout << d << std::endl; // address of c
  *const_cast<int*>(d) = 2; //undefined behaviour
  std::cout << c << std::endl; // prints 1
  std::cout << *d << std::endl; // but actually will print 2 on gadi
  std::cout << &c << std::endl; // address of c
  std::cout << &d << std::endl; // address of newly allocated variable

  return 0;
}
