*Note the reference code is in 1_constness_code.cpp*

A common source of confusion is being aware of what values are constant and which 
ones are able to be changed. Theoretically, this is very straightforward - 
anything which has the label `const` means a compiler error will be thrown if 
that object tries to be changed. For example,

```cpp
const int value = 1;
value = 2; // compile-time error
```

What if we have a constant pointer though?

```cpp
int a = 1;
const int *b = &a;
std::cout << *b << std::endl; // prints "1" to stdout

a = 2;
std::cout << *b << std::endl; // prints "2" to stdout

*b = 3; // this will throw a compiler error
```

So we see that we can't change anything with the pointer but we're able to change 
the underlying type completely fine. Is this the behaviour that we expect or want? 
Either way, this exists and we might have to do stuff that requires updating 
the underlying data while keeping the pointer itself intact (probably because 
a Trilinos object only gives us a const pointer but the underlying type isn't 
constant). 

Sometimes though we know our underlying value isn't const and it should be able 
to change but annoyingly Trilinos only gives us a const pointer. Fear not! 
Standard cpp has a function called const_cast which lets you get rid of the 
constness. Trilinos (or more specifically, Teuchos) has it's own smart pointers 
and ways of allocating and casting which I will go into in a sec but here's the 
standard cpp stuff first to set the basics.

```cpp
int a = 1;
const int *b = &a;

// *b = 2; // this throws an error
*const_cast<int*>(b) = 2; // this is ok
std::cout << *b << std::endl; // prints '2' to stdout
```

Now let's try something illegal.

```cpp
const int a = 1;
const int *b = &a;

*const_cast<int*>(b) = 2; // what happens?
```

Here we're casting away the constness of a pointer and attempting to change the 
underlying value which also happens to be constant. This is undefined behaviour 
and will differ from compiler to compiler and system to system. On Gadi, this 
will compile and run fine but our pointer will no longer point to the original 
const variable. It will point to a new region in memory with the new value 
assigned to it and the original variable will remain unchanged.  :) fun

This sounds like an awful lot of nitpicking but it's something to keep in mind 
when you suddenly can only deal with const values. Abusing this constness 
changing wouldn't be recommended but as a last resort when changing your entire 
codebase to accommodate something unexpected would take more effort than you
have, it's a pretty dirty trick.

Another thing on `const` (exasperrated sigh) is when it appears after a function 
signature or declaration. This means that the member function of the class can't 
change any member variables unless those variables are declared as mutable.
