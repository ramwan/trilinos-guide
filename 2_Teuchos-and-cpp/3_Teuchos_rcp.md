The Teuchos package in Trilinos provides a bunch of utilities but the most 
common one is definitely RCP (reference counted pointer) and its associated 
typecasting tools. It also provides objects for creating and accessing 
heirarchical lists (ParameterList) and a Teuchos version of the standard cpp 
std::vector called Array.

Here are some examples around RCP. Here I'm going to use some class C as a 
placeholder type.

```cpp
#include <Teuchos_RCPDecl.hpp>

int main()
{
  using Teuchos::RCP; // type
  using Teuchos::rcp; // function
  using Teuchos::null;

  RCP<C> c_ptr = rcp(new C);
  RCP<C> c_array_ptr = rcp( malloc(sizeof(C) * 6) ); // C array

  // We can then manually deallocate the memory by doing this
  c_ptr = null;
  c_array_ptr = null;
  // but we can also just leave the variable and when it leaves the scope, 
  // it'll be deallocated automatically like follows. Note that the curly braces 
  // create a scope for the inside lines.
  {
    RCP<C> c_ptr2 = rcp(new C);
    /* do stuff with int_ptr2 */
  }
  // c_ptr2 no longer exists here and our memory will have been freed.

  // We can also have constness
  RCP<const C> const_c = rcp(new const C);
  const RCP<const C> const_const_c = rcp(new const C);
  const RCP<C> const_c2 = rcp(new C);
}
```

This is probably the most basic common use cases. For some more examples, I'm 
going to refer you to [these examples](https://docs.trilinos.org/latest-release/packages/teuchos/doc/html/classTeuchos_1_1RCP.html#details) 
as I would basically be copying and pasting these examples here.
